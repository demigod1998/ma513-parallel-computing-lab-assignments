#include<bits/stdc++.h>
using namespace std;

int thread_count;
int n;
typedef complex<double> cd;
const double PI = 3.1415926536;
const complex<double> J(0,1);
cd w;
vector<cd> w_arr,arr2;
int m,d,logthread,logn;
struct args{
	int len;
	vector<cd> arr;
};

unsigned int reverse_bit(unsigned int c,int logn)
{
	int ans = 0;
	for(int i = 0;i < logn;i++)
	{
		ans = ans << 1;
		ans = ans |(c&1);
		c = c >> 1;
	}
	return ans;
}

void* f(void *arg)
{
	args *a = (args*)arg;
	vector<cd> R(n),S(n);
	S = a->arr;
	int j = a->len;
	int k = 1 << d;
	for(int i = 0 ; i < k;i++,j++)
	{
		int ind1 = j& ~(1<<(logn -1 -m));
		int ind2 = j | (1<<(logn -1 -m));
		int ind3 = j >> (logn -1 -m);
		ind3 = reverse_bit(ind3,m+1) << (logn -1 -m);

		arr2[j] = R[j] = S[ind1] + S[ind2]*w_arr[ind3];
	}
}

void fft(vector<cd> &arr,vector<cd>&arr_final,int logn,int logthread,bool inverse)
{
	vector<cd>R(n),S(n);
	for(int i = 0 ;i  <n ; i++)
	{
		R[i] = arr[i];
	}
	pthread_t tid[thread_count];
	vector<args> a(thread_count);

	if(inverse)w = exp(J*(-2*PI/n));

	for(int i = 1 ; i  < n ; i++)
		w_arr[i] = w_arr[i-1]*w;


	for(m = 0;m < logn ; m++)
	{
		for(int i = 0;i < n ; i++)
			arr2[i] = R[i];
		d = logn - logthread;
		for(int i = 0 ; i < thread_count ; i++)
		{
			a[i].len = i << d;
			a[i].arr = arr2;
		}
		for(int i = 0 ;i < thread_count ; i++)
		{
			pthread_create(&tid[i],NULL,f,(void*)(&a[i]));
		}
		for(int i = 0; i < thread_count ; i++)
		{
			pthread_join(tid[i],NULL);
		}

		for(int i = 0 ; i < thread_count ; i++)
		{
			int l = a[i].len;
			for(int k = 0;k < (1 << d) ; k++,l++)
				R[l] = arr2[l];
		}
	}

	for(unsigned int i = 0 ; i < n ; i++)
	{
		int ind = reverse_bit(i,logn);
		arr_final[i] = R[ind];
	}

}

int main(int argc,char **argv)
{
	thread_count = atoi(argv[1]);
	n = atoi(argv[2]);
	w = exp(J*(2*PI/n));
	logthread = log2(thread_count);
	logn = log2(n);
	for(int i = 0;i < n ; i++)
	{
		w_arr.push_back(1);
		arr2.push_back(1);
	}

	vector<cd> a(n),b(n),c(n),a_final(n),b_final(n),c_final(n);
	for(int i = 0; i < n/2 ; i++)
	{
		a[i] = i+1;
		b[i] = i+1;
	}
	struct timespec start, finish;
	double elapsed;
	clock_gettime(CLOCK_MONOTONIC, &start);

	fft(a,a_final,logn,logthread,false);
	fft(b,b_final,logn,logthread,false);
	for(int i = 0; i  < n ; i++)
	{
		c[i] = a_final[i]*b_final[i];
	}
	fft(c,c_final,logn,logthread,true);
	cd N = n*exp(0);
	for(int i = 0; i < n ; i++)
	{
		c_final[i] = c_final[i]/N;
	}
	clock_gettime(CLOCK_MONOTONIC, &finish);
	elapsed = (finish.tv_sec - start.tv_sec);
elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    cout << elapsed << endl;
    return 0;

}
