#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<pthread.h>
// using namespace std;
int *A;
int size_a,size_b;
int *B;
int *prod;
int part = 0;
int thread_count;
pthread_mutex_t *lock;
void *multi(void *arg)
{
	int thread_part = part++;
	for(int i = thread_part*(size_a/thread_count) ; i < (thread_part+1)*(size_a/thread_count);i++)
	{
		for(int j = 0 ; j < size_b ; j++)
		{
			pthread_mutex_lock(&lock[i+j]); 
			prod[i+j] += A[i]*B[j];
			pthread_mutex_unlock(&lock[i+j]); 
		}
	}
}

void initializer(int *x,int s,int val)
{
	for(int k = 0;k < s; k++)
	{
		*(x+k) = val;
	}
	return;
}
int main(int argc,char **argv)
{
	if(argc < 2)
	{
		printf("LESS ARGUMENTS\n");
		return -1;
	}
	struct timespec start, finish;
	double elapsed;
	thread_count = atoi(argv[1]);
	size_a = atoi(argv[2]);
	size_b = atoi(argv[2]);
	A = (int *)malloc(size_a*sizeof(int));
	B = (int *)malloc(size_b*sizeof(int));
	prod = (int *)malloc((size_a+size_b-1)*sizeof(int));
	lock = (pthread_mutex_t *)malloc((size_a+size_b-1)*sizeof(pthread_mutex_t));
	for(int k = 0; k < (size_a+size_b-1) ; k++)
	{
		if (pthread_mutex_init(&lock[k], NULL) != 0) { 
        printf("\n mutex init has failed\n"); 
        return 1; 
    } 
	}
	initializer(A,size_a,1);
	initializer(B,size_b,1);
	initializer(prod,size_a+size_b-1,0);

	pthread_t threads[thread_count];
	clock_gettime(CLOCK_MONOTONIC, &start);
	for(int i = 0 ;i  < thread_count ; i++)
	{
		pthread_create(&threads[i],NULL,multi,(void *)NULL);
	}
	for (int i = 0; i < thread_count ;i++) 
        pthread_join(threads[i], NULL);

    clock_gettime(CLOCK_MONOTONIC, &finish);
    elapsed = (finish.tv_sec - start.tv_sec);
elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    printf("%d\t%d\t%0.9f\n",thread_count,size_a,elapsed);

}
