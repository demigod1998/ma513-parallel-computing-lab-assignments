#!/bin/bash
gcc -pthread $1
for i in {2..8..1}
do
	./a.out "$(("2**${i}"))" "$(("2**16"))" >> $2_threads.txt
done
for i in {4..16..2}
do
	./a.out 16 "$(("2**${i}"))" >> $2_size.txt
done
wait
echo "DONE"
