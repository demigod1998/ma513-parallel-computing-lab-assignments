#include <bits/stdc++.h> 
using namespace std; 

struct util{
	string A;
	string B;
	int height;
};

int logthread;
int string_extender(string &A, string &B){
	int l1, l2;
	l1 = A.size(); 
	l2 = B.size(); 

	for (int i = 0 ; i < l2 - l1 ; i++) 
		A = '0' + A; 
	for (int i = 0 ; i < l1 - l2 ; i++) 
		B = '0' + B; 

	return max(l1,l2);
} 

string binary_string_sum(string a, string b){
	string result; 
	int l, bitA, bitB, c, bitSum;
	l = string_extender(a, b); 
	c = 0; 

	for (int i = l-1 ; i >= 0 ; i--) 
	{ 
		bitA = a.at(i)-'0'; 
		bitB = b.at(i)-'0'; 
		bitSum = (bitA ^ bitB ^ c)+'0'; 
		result = (char)bitSum + result; 
		c = (bitA & bitB) | (bitB & c) | (bitA & c); 
	} 
	if (c) 
		result = '1'+result; 

	return result; 
} 

int single_bit_calc(string A, string B){
	return (int)(A[0]-'0')*(int)(B[0]-'0'); 
} 

long int serial_karatsuba(string X, string Y) 
{ 
	int n = string_extender(X, Y); 
	if (n == 0) return 0; 
	if (n == 1) return single_bit_calc(X, Y); 

	int ll = n/2; 
	int rr = (n-ll);

	string Xl = X.substr(0, ll); 
	string Xr = X.substr(ll, rr); 

	string Yl = Y.substr(0, ll); 
	string Yr = Y.substr(ll, rr); 

	long int temp1 = serial_karatsuba(Xl, Yl); 
	long int temp2 = serial_karatsuba(Xr, Yr); 
	long int temp3 = serial_karatsuba(binary_string_sum(Xl, Xr), binary_string_sum(Yl, Yr)); 
	temp3 = (temp3 - temp1 - temp2);
	return temp1*(1<<(2*rr)) + temp3*(1<<rr) + temp2; 
} 

void* parallel_karatsuba(void* arguments){ 
	long int ans, P1, P2, P3;
	util *a = (util*)arguments;
	string X = a->A;
	string Y = a->B;
	pthread_t tid1, tid2, tid3;
	int n = string_extender(X, Y); 
	void *nul = NULL; 

	if (n == 0) 
		return (void*) 0;
	if (n == 1) {
		ans = single_bit_calc(X, Y); 
		return (void*) ans;
	}

	void *stat_1, *stat_2, *stat_3;
	util a1, a2, a3;

	int leftStrLen = n/2;
	int rightStrLen = (n-leftStrLen);

	a1.A = X.substr(0, leftStrLen); 
	a2.A = X.substr(leftStrLen, rightStrLen); 

	a1.B = Y.substr(0, leftStrLen); 
	a2.B = Y.substr(leftStrLen, rightStrLen); 

	a3.A = binary_string_sum(a1.A, a2.A);
	a3.B = binary_string_sum(a1.B, a2.B);

	a1.height = a->height + 1;
	a2.height = a->height + 1;
	a3.height = a->height + 1;

	if (a->height < logthread){
		pthread_create(&tid1, NULL, parallel_karatsuba, (void*)(&a1));
		pthread_create(&tid2, NULL, parallel_karatsuba, (void*)(&a2));
		pthread_create(&tid3, NULL, parallel_karatsuba, (void*)(&a3));
		pthread_join(tid1, &stat_1);
		pthread_join(tid2, &stat_2);
		pthread_join(tid3, &stat_3);
		P1 = (long int)stat_1;
		P2 = (long int)stat_2;
		P3 = (long int)stat_3;
	}
	else{
		P1 = serial_karatsuba(a1.A, a1.B); 
		P2 = serial_karatsuba(a2.A, a2.B); 
		P3 = serial_karatsuba(binary_string_sum(a1.A, a2.A), binary_string_sum(a1.B, a2.B)); 
	}

	ans = P1*(1<<(2*rightStrLen)) + (P3 - P1 - P2)*(1<<rightStrLen) + P2; 
	return (void*) ans;
} 

int main(int argc,char **argv){
	if(argc < 2)
	{
		cout << "LESS ARGUMENTS" << endl;
		return -1;
	}
	std::setprecision(10);
	int thread_count = atoi(argv[1]);
	int size_a = atoi(argv[2]);
	int size_b = atoi(argv[2]);
	logthread = log2(thread_count);
	void *status;
	pthread_t tid1;
	struct timespec start, finish;
	double elapsed;
	util a;
	a.A = a.B = "";
	for (int i=0; i<size_a; i++)
	{
		a.A = a.A + '1';
	}	
	for (int i=0; i<size_b; i++)
	{
		a.B = a.B + '1';
	}	
	a.height = 0;

	clock_gettime(CLOCK_MONOTONIC, &start);

	pthread_create(&tid1, NULL, parallel_karatsuba, (void*)(&a));
	pthread_join(tid1, &status);
	// long int ans = (long int)status;

	clock_gettime(CLOCK_MONOTONIC, &finish);

	// cout<<ans<<endl;

	// time_taken = (end-begin)/timer;
	// cout<< time_taken << endl;
	elapsed = (finish.tv_sec - start.tv_sec);
elapsed += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;
    cout << elapsed << endl;
	return 0;
} 
