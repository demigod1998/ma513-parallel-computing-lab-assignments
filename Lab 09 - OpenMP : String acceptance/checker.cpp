#include<bits/stdc++.h>
#include<omp.h>
#include<sys/time.h>
using namespace std;

string generator(long int string_length)
{
	int r = rand()%2;
	long int div = rand()%(string_length-1) + 1;
	string ans;
	ans.resize(string_length,'0');
	if(1)
	{
		// gauranteed accept
		if(div%2 != 0)div--;

		for(long int i = 0;i < div;i++)
		{
			ans[i] = 'a';
		}
		for(long int i = div ; i < string_length ; i++)
		{
			ans[i] = 'b';
		}

	}
	else
	{
		// gauranteed reject
		int r2 = rand()%3;
		if(r2 == 1)
		{
		// a is odd
			if(div%2 == 0)div--;

			for(long int i = 0;i < div;i++)
			{
				ans[i] = 'a';
			}
			for(long int i = div ; i < string_length ; i++)
			{
				ans[i] = 'b';
			}
		}
		else if(r2 == 2)
		{
			// b*a*
			for(long int i = 0;i < div;i++)
			{
				ans[i] = 'b';
			}
			for(long int i = div ; i < string_length ; i++)
			{
				ans[i] = 'a';
			}			
		}
		else
		{
			for(long int i = 0 ;i < string_length ; i++)
			{
				ans[i] = char('a' + rand()%2);
			}
		}
	}

	return ans;
}

bool is_state(int state)
{
	if(state >= 0 && state <= 4)return 1;

	cout <<"ERROR :: UNIDENTIFIED STATE" << endl;
	return -1;
}

bool is_action(char a)
{
	if(a=='a' || a == 'b')return 1;

	cout <<"ERROR :: UNIDENTIFIED ACTION" << endl;
	return -1;
}

int next_state(int state,char action)
{
	if(!is_state(state) || !is_action(action)) return -1;

// state = 4 is reject state hence no out possible
// state = 3 in accept state
	if(state == 0)
	{
		if(action == 'a')
		{
			return 1;
		}
		else if(action == 'b')
		{
			return 3;
		}
		else
		{
			// cout << state << " " << action << "\tthrow error"<<endl;
			return -1;
		}
	}
	else if(state == 1)
	{
		if(action == 'a')
		{
			return 2;
		}
		else if(action == 'b')
		{
			return 4;
		}
		else
		{
			// cout << state << " " << action << "\tthrow error"<<endl;
			return -1;
		}	
	}
	else if(state == 2)
	{
		if(action == 'a')
		{
			return 1;
		}
		else if(action == 'b')
		{
			return 3;
		}
		else
		{
			// cout << state << " " << action << "\tthrow error"<<endl;
			return -1;
		}	
	}
	else if(state == 3)
	{
		if(action == 'a')
		{
			return 4;
		}
		else if(action == 'b')
		{
			return 3;
		}
		else
		{
			// cout << state << " " << action << "\tthrow error"<<endl;
			return -1;
		}	
	}
	else if(state == 4)
	{
		if(action == 'a')
		{
			return 4;
		}
		else if(action == 'b')
		{
			return 4;
		}
		else
		{
			// cout << state << " " << action << "\tthrow error"<<endl;
			return -1;
		}	
	}

	// cout << state << " " << action << "\tthrow error"<<endl;
	return -1;
}

int process_string(int state, string &actions,long int start,long int end)
{
	int temp = state;
	for(long int i = start;i < end ;i++)
	{
		temp = next_state(temp,actions[i]);

		// if already in end state then just end the processing
		if(temp == 4)break;
	}
	return temp;
}

int parallel_processor(int thread_count,string &actions)
{
	long int string_length = actions.size();
	omp_set_num_threads(thread_count);
	unordered_map<int,vector<int> > dict;
	// int *dict[thread_count];
	long int per_thread = string_length/thread_count;
	// cout << "check1 : " <<per_thread << endl;
	int jump = 0;
	int pid;
	long int start,end;
	int x = 1;
	#pragma omp parallel private(pid,start,end,x)
	{
		pid = omp_get_thread_num();
		start = pid*per_thread;
		end = (pid+1)*per_thread;
		// #pragma omp atomic
		// {
		// 	cout << "check2 : " <<pid << " " << start << " " << end << endl;	
		// }	
		if(pid == thread_count-1)end = string_length;

		if(pid == 0)
		{
			//basic thread
			jump = process_string(0,actions,start,end);

		}
		else
		{
			vector<int> possible(5);
			possible[0] = 4;
			for(int i = 1;i<4;i++)
			{
				possible[i] = process_string(i,actions,start,end);	
			}
			possible[4] = 4;
			dict[pid] = possible;
		}

		if (x!=0) {
 		  #pragma omp barrier    /* valid usage    */
		}
	}
	for(int i = 1 ; i < thread_count; i++)
	{
		if(jump == 4)break;

		if(jump == -1)cout << "ERROR :: -1 seen in parallel_processor" << endl;
		
		jump = dict[i][jump];
	}

	return jump;
}
int main(int argc,char** argv)
{
	cout.precision(10);
	if(argc < 3)
	{
		cout << "LESS ARGUMENTS" << endl;
		return -1;
	}
	srand(0);
	// vector<float> pt,st;
	int thread_count = atoi(argv[1]); // for an aditional simple thread that processes the start
	int string_length = atoi(argv[2]);
	// int thread_count = 6; // for an aditional simple thread that processes the start
	// int string_length = 8;
	
	// int t = 10;
	// while(t--)
	{
		
		string actions = generator(string_length);

		struct timeval seqs,seqe,pars,pare;
		gettimeofday(&seqs,NULL);
		int sequential_state = process_string(0,actions,0,string_length);
		gettimeofday(&seqe,NULL);

		gettimeofday(&pars,NULL);
		int parallel_state = parallel_processor(thread_count,actions);
		gettimeofday(&pare,NULL);

		if(parallel_state == sequential_state)
		{
			float time_serial = (float)((float)((seqe.tv_sec * 1000000 + seqe.tv_usec)
	                - (seqs.tv_sec * 1000000 + seqs.tv_usec))/(float)1000);
	        float time_parallel = (float)((float)((pare.tv_sec * 1000000 + pare.tv_usec)
	                - (pars.tv_sec * 1000000 + pars.tv_usec))/(float)1000);
	        // time_serial = time_serial / time_parallel;
	        cout << thread_count << " " << string_length << " " << time_serial << " " << time_parallel << endl;
	        // pt.push_back(time_parallel);
	        // st.push_back(time_serial);
		}
		else
		{
			cout << "ERROR :: DIFFERENT STATE REACHED FOR INPUT " << actions << " " << sequential_state << " " << parallel_state << endl;
			// continue;
		}
	}
	// auto n = min(pt.size(),st.size()); 
	// float s_average = 0.0f;
	// float p_average = 0.0f;
	// if ( n != 0) {
	//      s_average = accumulate( st.begin(), st.begin()+n, 0.0) / n;
	//      p_average = accumulate( pt.begin(), pt.begin()+n, 0.0) / n; 
	// }
	// cout << thread_count-1 << " " << string_length << " " <<s_average << " " << p_average << endl;
return 0;
}
