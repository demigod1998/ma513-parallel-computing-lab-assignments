#include<bits/stdc++.h>
#include<omp.h>
#include<sys/time.h>
using namespace std;
map<pair<int,char> , int> m;
int end_state;
int start_state;
int count_state;

int findMax(set<int> &my_set) 
{ 
  
    // Get the maximum element 
    int max_element; 
    if (!my_set.empty()) 
        max_element = *my_set.rbegin(); 
  
    // return the maximum element 
    return max_element; 
} 
  
// Function to find the minimum element 
int findMin(set<int> &my_set) 
{ 
  
    // Get the minimum element 
    int min_element; 
    if (!my_set.empty()) 
        min_element = *(--my_set.rend()); 
  
    // return the minimum element 
    return min_element; 
}
int process_string(int state, string &actions,long int start,long int end)
{
	int temp = state;
	for(long int i = start;i < end ;i++)
	{
		if(temp == end_state)break;


		temp = m[{temp,actions[i]}];
		// if already in end state then just end the processing
	}
	return temp;
}


int parallel_processor(int thread_count,string &actions)
{
	long int string_length = actions.size();
	omp_set_num_threads(thread_count);
	unordered_map<int,vector<int> > dict;
	long int per_thread = string_length/thread_count;
	int jump = 0;
	int pid;
	long int start,end;
	int x = 1;
	#pragma omp parallel private(pid,start,end,x)
	{
		pid = omp_get_thread_num();
		start = pid*per_thread;
		end = (pid+1)*per_thread;
		if(pid == thread_count-1)end = string_length;

		if(pid == 0)
		{
			jump = process_string(0,actions,start,end);
		}
		else
		{
			vector<int> possible(count_state);
			for(int i = start_state;i<= end_state;i++)
			{
				if(i == start_state || i == end_state)possible[i] = end_state;
				else
				{
					possible[i] = process_string(i,actions,start,end);
				}	
			}
			dict[pid] = possible;
		}

		if (x!=0) {
 		  #pragma omp barrier    /* valid usage    */
		}
	}
	for(int i = 1 ; i < thread_count; i++)
	{
		if(jump == 4)break;

		if(jump == -1)cout << "ERROR :: -1 seen in parallel_processor" << endl;
		
		jump = dict[i][jump];
	}

	return jump;
}

int main()
{
	cout.precision(10);
	srand(0);
	int thread_count;
	int num_edges;
	cin >> thread_count >> num_edges;
	set<int> temp;
	for(int i = 0; i < num_edges; i++)
	{
		int a,b;
		char c;
		cin >> a >> b >> c;
		m[make_pair(a,c)] = b;
		temp.insert(a);
		temp.insert(b);
	}
	string s;
	cin >> s;
	
	start_state =  findMin(temp);
	end_state = findMax(temp);
	count_state = temp.size();

	struct timeval seqs,seqe,pars,pare;
	gettimeofday(&seqs,NULL);
	int sequential_state = process_string(0,s,0,s.size());
	gettimeofday(&seqe,NULL);

	gettimeofday(&pars,NULL);
	int parallel_state = parallel_processor(thread_count,actions);
	gettimeofday(&pare,NULL);

	if(parallel_state == sequential_state)
	{
		float time_serial = (float)((float)((seqe.tv_sec * 1000000 + seqe.tv_usec)
                - (seqs.tv_sec * 1000000 + seqs.tv_usec))/(float)1000);
        float time_parallel = (float)((float)((pare.tv_sec * 1000000 + pare.tv_usec)
                - (pars.tv_sec * 1000000 + pars.tv_usec))/(float)1000);
        // time_serial = time_serial / time_parallel;
        cout << thread_count << " " << string_length << " " << time_serial << " " << time_parallel << endl;
        // pt.push_back(time_parallel);
        // st.push_back(time_serial);
	}
	else
	{
		cout << "ERROR :: DIFFERENT STATE REACHED FOR INPUT " << actions << " " << sequential_state << " " << parallel_state << endl;
		// continue;
	}

	return 0;
}
