#include<stdio.h>
#include<stdlib.h>
#include<omp.h>

int main(int argc,char **argv)
{
	if(argc < 3)
	{
		printf("Not enough arguments\n");
		return -1;
	}
	int Nsteps,Nthreads;
	double temp,pi,ans;
	int i = 0;
	temp = 0.0;
	pi = ans = temp;
	double start_time,end_time;
	Nthreads = atoi(argv[1]);
	Nsteps = atoi(argv[2]);
	double step_size = 1.0/(double)Nsteps;
	omp_set_num_threads(Nthreads);
	start_time = omp_get_wtime();
	// specifying to parallelize over the for loop where i,temp are private variables
	// local copy of ans construct for each thread is made and is initialized to 0 as we are doind addition
	// local copies are reduced to single value internally and finally combined with the original value
	#pragma omp parallel for private(i,temp) reduction(+:ans)
		for(i = 0;i<Nsteps;i++)
		{
			temp = (i+0.5)*step_size;
			ans += 4.0/(1.0 + temp*temp);
		}
	
	pi = ans*step_size;	
	end_time = omp_get_wtime();
	printf("%d\t%d\t%0.8f\n",Nthreads,Nsteps,end_time-start_time);
	return 0;
}
