#!/bin/bash
gcc $1 -fopenmp
for i in {1..6..1}
do
	./a.out "$(("2**${i}"))" 100000 >> $2_threads.txt
done
for i in {1..8..1}
do
	./a.out 8 "$(("10**${i}"))" >> $2_steps.txt
done
wait
echo "DONE"
