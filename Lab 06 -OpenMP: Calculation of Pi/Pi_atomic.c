#include<omp.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc,char **argv)
{
	if(argc < 3)
	{
		printf("Not enough arguments\n");
		return -1;
	}
	double pi = 0.0;
	double start_time,end_time;
	int Nthreads,Nsteps;
	Nthreads = atoi(argv[1]);
	Nsteps = atoi(argv[2]);
	double step_size = 1.0/(double)Nsteps;
	omp_set_num_threads(Nthreads);
	start_time = omp_get_wtime();
	#pragma omp parallel
	{
		int id,nthrds;
		id = omp_get_thread_num();
		nthrds = omp_get_num_threads();
		double temp,ans;
		temp = 0.0;
		ans = temp;
		for(int i = id; i < Nsteps; i+= nthrds)
		{
			temp = (i+0.5)*step_size;
			ans += 4.0/(1.0 + temp*temp);
		}
		#pragma omp atomic
			pi += ans*step_size;
	}
	end_time = omp_get_wtime();
	printf("%d\t%d\t%0.8f\n",Nthreads,Nsteps,end_time-start_time);
	return 0;
}
