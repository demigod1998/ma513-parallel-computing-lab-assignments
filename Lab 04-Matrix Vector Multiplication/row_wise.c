// mpicc row_wise.c
// mpirun -n #process ./a.out n
#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>

int main(int argc,char **argv)
{
	MPI_Init(&argc,&argv);

	int Nprocesses,pid;
	MPI_Comm_size(MPI_COMM_WORLD,&Nprocesses);
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);

	int n = atoi(argv[1]);
	int perProw = n/Nprocesses;

	//Assuming each process has its part of perProw rows of A&b

	int arr[perProw][n];
	for(int i = 0;i<perProw;i++)
	{
		for(int j = 0;j < n ; j++)
		{
			arr[i][j] = 1;
		}
	}

	int b[perProw];
	for(int j = 0;j < perProw ; j++)
	{
		b[j] = 1;
	}
	MPI_Barrier(MPI_COMM_WORLD);
	double start = MPI_Wtime();

	// we need to get fragments of b from other processes
	int b_final[n];
	MPI_Allgather(b,perProw,MPI_INT,b_final,perProw,MPI_INT,MPI_COMM_WORLD);

	// now doing the multiplication
	for(int i = 0;i < perProw ;i++)
	{
		b[i] = 0;
		for(int j = 0;j < n ; j++)
		{
			b[i] += arr[i][j]*b_final[j];
		}
	}

	// now in b each process has its perProw number of elements of b after the multiplication

	double end = MPI_Wtime();
	double min_s,max_e;
	MPI_Reduce(&start,&min_s,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
	MPI_Reduce(&end,&max_e,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

	if(pid == 0)
	{
		double timetaken = max_e - min_s;
		printf("%0.8f\n",timetaken);
	}

	MPI_Finalize();
	return 0;
}
