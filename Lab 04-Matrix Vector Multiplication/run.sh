#!/bin/bash
mpicc $1
for i in {2..6..2}
do
	mpirun -n "$(("2**${i}"))" ./a.out 1024 >> $2_processes.txt
done

for i in {1..4..1}
do
	mpirun -n 16 ./a.out "$(("4**${i}"))" >> $2_size.txt
done
wait
echo "DONE"
