// mpicc block_wise.c
// mpirun -n #process ./a.out n
#include<mpi.h>
#include<stdio.h>
#include<stdlib.h>

int floorSqrt(int x)
{
	if(x <= 0)return x;

	int s = 1;
	int e = x;
	int ans;
	while(s <= e)
	{
		int m = (s+e)/2;
		if(m*m == x)return m;
		else if(m*m < x)
		{
			s = m+1;
			ans = m; // because we need floor
		}
		else
		{
			e = m-1;
		}
	}

	return ans;
}

int main(int argc, char **argv)
{
	MPI_Init(&argc,&argv);

	int Nprocess;
	int pid;
	MPI_Comm_size(MPI_COMM_WORLD,&Nprocess);
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);

	int n = atoi(argv[1]);
	int rootp = floorSqrt(Nprocess);
	int per_process = n/rootp;

	//Array A- assuming each proess has its share of A as arr block
	int arr[per_process][per_process];
	for(int i = 0;i < per_process;i++)
	{
		for(int j = 0; j < per_process ; j++)
		{
			arr[i][j] = 1;
		}
	}

	int *b = NULL;
	if(pid%rootp == rootp - 1)
	{
		b = (int *)malloc(per_process * sizeof(int));
		for(int i = 0 ; i < per_process ; i++)
		{
			b[i] = 1;
		}
	}
	MPI_Barrier(MPI_COMM_WORLD);
	double start_time = MPI_Wtime();

	//step 1 : send the blocks of b to first row processes
	if(pid%rootp == rootp - 1)
	{
		int d = pid/rootp;
		MPI_Ssend(b,per_process,MPI_INT,d,0,MPI_COMM_WORLD);
	}

	int *recb = NULL;
	recb = (int *)malloc(per_process*sizeof(int));
	if(pid <= rootp - 1)
	{
		int src = (pid+1)*rootp - 1;

		MPI_Recv(recb,per_process,MPI_INT,src,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}

	//step 2: creating a column communicator and Bcast along the column
 	MPI_Comm colCom;
	MPI_Comm_split( MPI_COMM_WORLD, pid%rootp,pid,&colCom);

	MPI_Bcast(recb,per_process,MPI_INT,0,colCom);
	MPI_Comm_free(&colCom);

	//now we need to partial computations
	int b_temp[per_process];
	for(int i = 0;i < per_process; i++)
	{
		b_temp[i] = 0;
		for(int j = 0;j < per_process; j++)
		{
			b_temp[i] += arr[i][j]*recb[j];
		}

	}

	//step 3: all to one reduction of partial results along row

	MPI_Comm rowCom;
	MPI_Comm_split(MPI_COMM_WORLD,pid/rootp,pid*(-1),&rowCom);

	MPI_Reduce(b_temp,b,per_process,MPI_INT,MPI_SUM,0,rowCom);
	MPI_Comm_free(&rowCom);

	double end_time = MPI_Wtime();
	double s,e;
	MPI_Reduce(&start_time,&s,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
	MPI_Reduce(&end_time,&e,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

	if(pid == 0)
	{
		double timeTaken = e-s;
		printf("%0.9f\n",timeTaken);
	}

	MPI_Finalize();
	return 0;

}
