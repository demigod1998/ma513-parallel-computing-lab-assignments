#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>

int main(int argc,char **argv)
{
	MPI_Init(&argc,&argv);
	int Nprocesses,pid;
	MPI_Comm_size(MPI_COMM_WORLD,&Nprocesses);
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);

	int n = atoi(argv[1]);
	int perPcol = n/Nprocesses;

	int arr[n][perPcol];
	int b[perPcol];
	for(int i = 0 ; i < n ; i++)
	{
		for(int j = 0 ; j < perPcol ; j++)
		{
			arr[i][j] = 1;
			b[j] = 1;
		}
	}


	MPI_Barrier(MPI_COMM_WORLD);
	double start = MPI_Wtime();

	int temp_b[n];
	for(int i = 0 ; i < n ; i++)
	{
		temp_b[i] = 0;
		for(int j = 0 ; j < perPcol ; j++)
		{
			temp_b[i] += arr[i][j]*b[j];
		}
	}	

	int *b_final;
	if(pid == 0)
	{
		b_final = (int *)malloc(n*sizeof(int));
	}

	MPI_Reduce(temp_b,b_final,n,MPI_INT,MPI_SUM,0,MPI_COMM_WORLD);

	MPI_Scatter(b_final,perPcol,MPI_INT,b,perPcol,MPI_INT,0,MPI_COMM_WORLD);
	
	double end = MPI_Wtime();
	double min_s,max_e;
	MPI_Reduce(&start,&min_s,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
	MPI_Reduce(&end,&max_e,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

	if(pid == 0)
	{
		double timetaken = max_e - min_s;
		printf("%0.8f\n",timetaken);
	}

	MPI_Finalize();
	return 0;
}
