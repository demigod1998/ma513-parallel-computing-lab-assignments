#include<bits/stdc++.h>
#include<mpi.h>
using namespace std;


int main(int argc,char** argv)
{
	MPI_Init(&argc,&argv);

	int process_count;
	MPI_Comm_size(MPI_COMM_WORLD,&process_count);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);

	int n = atoi(argv[1]);
	double *arr, f;
	double st,et,tt;

	int per_process = n/process_count;

	double *temp_row = new double[n];

	if(pid == 0)
	{
		arr = new double[n*n];
		for(int i = 0;i < n ; i++)
		{
			for(int j = 0;j < n;j++)
			{
				arr[i*n + j] = (double(rand())/double(RAND_MAX)) * (100 - -100) + -100;
			}
		}

		//cout << "check 1" << endl;
	}

	double *recv = new double[n * per_process];
	MPI_Scatter(arr,n*per_process,MPI_DOUBLE,recv,n*per_process,MPI_DOUBLE,0,MPI_COMM_WORLD);

	MPI_Barrier(MPI_COMM_WORLD);

	if(pid == 0){
		//cout << "check 2" << endl;
		st = MPI_Wtime();
	}
	for(int i = 0; i < n ; i++)
	{
		int k = i/process_count;
		if(i%process_count == pid)
		{
			//cout << pid << "check 3" << endl;
			for(int j = i+1; j < n; j++)
			{
				recv[k*n + j] /= recv[k*n + i];
			}
			recv[k*n + i] = 1;

			memcpy(temp_row,&recv[k*n],n*sizeof(double));

			MPI_Send(temp_row,n,MPI_DOUBLE,(pid+1)%process_count, 0, MPI_COMM_WORLD);

			for(int r = k + 1; r < per_process ; r++)
			{
				f = recv[r*n+i];
				for(int c = i +1; c < n ; c++)
				{
				recv[r*n + c] = recv[r*n + c] - f*temp_row[c];
				}
				recv[r*n+i] = 0;
			}
		}
		else
		{
			MPI_Recv(temp_row,n,MPI_DOUBLE,(pid-1)%process_count,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);

			//cout << pid << "check 7" << endl;
			if((i-1)%process_count != pid)
			{
				MPI_Send(&recv[k*n],n,MPI_DOUBLE,(pid+1)%process_count,0,MPI_COMM_WORLD);
			}
			//cout << pid << "check 8" << endl;
			int q = k;
			if((i%process_count) >= pid)q++;

			for(int r = q;r < per_process;r++)
			{
				f = recv[r*n+i];
				for(int c = i+1; c < n ; c++)
				{
				recv[r*n + c] = recv[r*n + c] - f*temp_row[c];
				}

				recv[r* n + i] = 0;
			}

			//cout << pid << "check 4" << endl;
		}
	}
	//cout << pid << "\tcheck 6" << endl;
	MPI_Barrier(MPI_COMM_WORLD);
	if(pid == 0)
	{
		//cout << pid << "check 5" << endl;
		et = MPI_Wtime();
		tt = et - st;
		cout << tt << endl;
	}

	MPI_Finalize();

}
