#include<stdio.h>
#include<mpi.h>


int main(int argc,char** argv)
{
	MPI_Init(&argc,&argv);
	int p_count;
	MPI_Comm_size(MPI_COMM_WORLD,&p_count);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);
	int d = -1;
	if(pid)
	{	
		printf("\nPE %d, original data : %d",pid,d);
		MPI_Recv(&d,1,MPI_INT,0,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		printf("\nPE %d, received data : %d",pid,d);
		d = pid;
		MPI_Ssend(&d,1,MPI_INT,0,1,MPI_COMM_WORLD);
		printf("\nPE %d, sent data : %d",pid,d);
	}
	else
	{
		printf("\nPE %d, original data : %d",pid,d);
		MPI_Recv(&d,1,MPI_INT,1,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		printf("\nPE %d, received data : %d",pid,d);
		d = pid;
		MPI_Ssend(&d,1,MPI_INT,1,1,MPI_COMM_WORLD);
		printf("\nPE %d, sent data : %d",pid,d);	
	}


	MPI_Finalize();
	return 0;
}
