//A basic code showing process ids and prints HELLO/WORLD based on rank
// TO run : 
//mpicc hello_world.c
//mpirun -n #processes ./a.out
#include<stdio.h>
#include<mpi.h>


int main(int argc,char** argv)
{
	MPI_Init(&argc,&argv);
	int p_count;
	MPI_Comm_size(MPI_COMM_WORLD,&p_count);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);

	if(pid%2)
	{
		printf("PE %d WORLD\n",pid);
	}
	else
	{
		printf("PE %d HELLO\n",pid);
	}

	MPI_Finalize();
	return 0;
}
