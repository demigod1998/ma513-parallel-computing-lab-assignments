g++ -o matrix matrix_multiply.cpp
./matrix < input1.txt > output_size.txt
./matrix < input2.txt > output_processes.txt
g++ -O1 -o matrix1 matrix_multiply.cpp
./matrix1 < input1.txt > output_size1.txt
./matrix1 < input2.txt > output_processes1.txt
g++ -O2 -o matrix2 matrix_multiply.cpp
./matrix2 < input1.txt > output_size2.txt
./matrix2 < input2.txt > output_processes2.txt
g++ -O3 -o matrix3 matrix_multiply.cpp
./matrix3 < input1.txt > output_size3.txt
./matrix3 < input2.txt > output_processes3.txt
g++ -Os -o matrixs matrix_multiply.cpp
./matrixs < input1.txt > output_sizes.txt
./matrixs < input2.txt > output_processess.txt
g++ -Ofast -o matrixf matrix_multiply.cpp
./matrixf < input1.txt > output_sizef.txt
./matrixf < input2.txt > output_processesf.txt
