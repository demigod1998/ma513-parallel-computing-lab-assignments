#include<iostream>
#include<cstdlib>
#include <chrono>
using namespace std::chrono;
using namespace std;

void setter(double **A,int &n,int &a)
{
	for(int i = 0 ; i < n; i++)
	{
		for(int j = 0 ; j < n ; j++)A[i][j] = a;
	}
}

void printer(double **A, int &n)
{
  for(int i = 0 ; i < n; i++)
	{
		for(int j = 0 ; j < n ; j++)
    {
      if(A[i][j] != n)
      {
        cout <<"wrong multiply"<<endl;
				for(int i = 0 ; i < n; i++)
				{
					for(int j = 0 ; j < n ; j++)
			    {
						cout <<A[i][j]<<" ";
					}
					cout << endl;
				}
        return;
      }
    }
	}
}

void naive_multiply(double **A,double **B,double **C, int &n)
{
  for(int i = 0; i < n; i++)
  {
    for(int j = 0; j < n; j++)
    {
      for(int k = 0; k < n; k++)
      {
        C[i][j] += A[i][k]*B[k][j];
      }
    }
  }
  return;
}

void loop_exchange(double **A,double **B,double **C, int &n)
{
  for(int i = 0; i < n; i++)
  {
    for(int k = 0; k < n; k++)
    {
      for(int j = 0; j < n; j++)
      {
        C[i][j] += A[i][k]*B[k][j];
      }
    }
  }
  return;
}

void block_multiply(double **A,double **B,double **C, int &n,int &block_size)
{
  for(int bi = 0; bi < n; bi += block_size)
  {
    for(int bj = 0; bj < n; bj += block_size)
    {
      for(int bk = 0; bk < n; bk += block_size)
      {
        for(int i = 0; i < block_size; i++)
        {
          for(int j = 0; j < block_size; j++)
          {
            for(int k = 0; k < block_size; k++)
            {
              C[i+bi][j+bj] += A[i+bi][k+bk]*B[k+bk][j+bj];
            }
          }
        }
      }
    }
  }
  return;
}

void block_multiply_with_loop_exchange(double **A,double **B,double **C, int &n,int &block_size)
{
  for(int bi = 0; bi < n; bi += block_size)
  {
    for(int bj = 0; bj < n; bj += block_size)
    {
      for(int bk = 0; bk < n; bk += block_size)
      {
        for(int i = 0; i < block_size; i++)
        {
          for(int k = 0; k < block_size; k++)
          {
            for(int j = 0; j < block_size; j++)
            {
              C[i+bi][j+bj] += A[i+bi][k+bk]*B[k+bk][j+bj];
            }
          }
        }
      }
    }
  }
  return;
}

void recurssive_multiply(double **A,double **B,double **C, int ax1,int ay1,int ax2,int ay2,int bx1,int by1,int bx2,int by2,int cx1,int cy1,int cx2,int cy2)
{
  if(abs(ax1-ax2) == 0)
  {
		C[cx1][cy1] += A[ax1][ay1] * B[bx1][by1];
  }
	else
	{
		int ax,ay,bx,by,cx,cy;
		ax = (ax2+ax1+1)/2;
		ay = (ay2+ay1+1)/2;
		bx = (bx2+bx1+1)/2;
		by = (by2+by1+1)/2;
		cx = (cx2+cx1+1)/2;
		cy = (cy2+cy1+1)/2;
		//C1
		recurssive_multiply(A,B,C,ax1,ay1,ax-1,ay-1,bx1,by1,bx-1,by-1,cx1,cy1,cx-1,cy-1);//A1 B1
		recurssive_multiply(A,B,C,ax1,ay,ax-1,ay2,bx,by1,bx2,by-1,cx1,cy1,cx-1,cy-1);// A2 B3
		//C2
		recurssive_multiply(A,B,C,ax1,ay1,ax-1,ay-1,bx1,by,bx-1,by2,cx1,cy,cx-1,cy2);//A1 B2
		recurssive_multiply(A,B,C,ax1,ay,ax-1,ay2,bx,by,bx2,by2,cx1,cy,cx-1,cy2);//A2 B4
		//C3
		recurssive_multiply(A,B,C,ax,ay1,ax2,ay-1,bx1,by1,bx-1,by-1,cx,cy1,cx2,cy-1);//A3 B1
		recurssive_multiply(A,B,C,ax,ay,ax2,ay2,bx,by1,bx2,by-1,cx,cy1,cx2,cy-1);	//A4 B3
		//C4
		recurssive_multiply(A,B,C,ax,ay1,ax2,ay-1,bx1,by,bx-1,by2,cx,cy,cx2,cy2);//A3 B2
		recurssive_multiply(A,B,C,ax,ay,ax2,ay2,bx,by,bx2,by2,cx,cy,cx2,cy2);//A4 B4
	}

	return;
}

int main()
{
	int p;
	cin >> p;
	while(p--)
	{
	int n,a = 1,c = 0,block_size;
	cin >> n;
  cin >> block_size;
  cout <<endl;
	double **A = (double **)malloc(n * sizeof(double *));
	for(int i = 0 ; i < n; i++)
	{
		A[i] = (double *)malloc(n * sizeof(double));
	}
	double **B = (double **)malloc(n * sizeof(double *));
	for(int i = 0 ; i < n; i++)
	{
		B[i] = (double *)malloc(n * sizeof(double));
	}
	double **C = (double **)malloc(n * sizeof(double *));
	for(int i = 0 ; i < n; i++)
	{
		C[i] = (double *)malloc(n * sizeof(double));
	}
  auto start = std::chrono::high_resolution_clock::now();
  auto finish = start;
  std::chrono::duration<double> elapsed1 = finish - start;
	std::chrono::duration<double> elapsed2 = finish - start;
	std::chrono::duration<double> elapsed3 = finish - start;
	std::chrono::duration<double> elapsed4 = finish - start;
	std::chrono::duration<double> elapsed5 = finish - start;

  // A,B,C matrices are n*n 2D matrices
	setter(A,n,a);
	setter(B,n,a);
	setter(C,n,c);
  // cout<< "Naive Multiplication--------------" << endl;
  start = std::chrono::high_resolution_clock::now();
  naive_multiply(A,B,C,n);
  finish = std::chrono::high_resolution_clock::now();
	elapsed1 = finish - start;
	// cout << "Elapsed time: " << elapsed.count() << " s\n";
  printer(C,n);
  setter(A,n,a);
	setter(B,n,a);
  setter(C,n,c);
  // cout<< "Loop Exchange Multiplication-------" << endl;
  start = std::chrono::high_resolution_clock::now();
  loop_exchange(A,B,C,n);
  finish = std::chrono::high_resolution_clock::now();
	elapsed2 = finish - start;
	// cout << "Elapsed time: " << elapsed.count() << " s\n";
  printer(C,n);

  setter(A,n,a);
	setter(B,n,a);
  setter(C,n,c);
  // cout<< "Block Multiplication-------" << endl;
  start = std::chrono::high_resolution_clock::now();
  block_multiply(A,B,C,n,block_size);
  finish = std::chrono::high_resolution_clock::now();
	elapsed3 = finish - start;
	// cout << "Elapsed time: " << elapsed.count() << " s\n";
  printer(C,n);

  setter(A,n,a);
	setter(B,n,a);
  setter(C,n,c);
  // cout<< "Block Multiplication with loop Exchange-------" << endl;
  start = std::chrono::high_resolution_clock::now();
  block_multiply_with_loop_exchange(A,B,C,n,block_size);
  finish = std::chrono::high_resolution_clock::now();
	elapsed4 = finish - start;
	// cout << "Elapsed time: " << elapsed.count() << " s\n";
  printer(C,n);

  setter(A,n,a);
	setter(B,n,a);
  setter(C,n,c);
  // cout<< "Recurrsive Multiplication-------" << endl;
  start = std::chrono::high_resolution_clock::now();
  recurssive_multiply(A,B,C,0,0,n-1,n-1,0,0,n-1,n-1,0,0,n-1,n-1);
  finish = std::chrono::high_resolution_clock::now();
	elapsed5 = finish - start;
	// cout << "Elapsed time: " << elapsed.count() << " s\n";
  printer(C,n);
	cout << n << " " << block_size << " " << elapsed1.count() << " " << elapsed2.count() << " " << elapsed3.count() << " " << elapsed4.count() << " " << elapsed5.count() << endl;
	}
	return 0;
}
