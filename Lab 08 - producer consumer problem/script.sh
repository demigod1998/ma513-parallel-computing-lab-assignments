#!/bin/bash
g++ $1 -pthread -latomic 

for i in {1..8..1}
do
	./a.out "$(("2**${i}"))" 32 32 >> $2_queue_size.txt
done


for i in {1..8..1}
do
	./a.out 128 "$(("2**${i}"))" 32 >> $2_producer.txt
done

for i in {1..8..1}
do
	./a.out 128 32 "$(("2**${i}"))" >> $2_consumer.txt
done
wait
echo "DONE"

