#include<bits/stdc++.h>
#include<pthread.h>  
#include<atomic>

using namespace std;

int msq_size;
int eq_ops = 0;
int dq_ops = 0;
struct node;
struct pointer{
	node* ptr;
	int index;

    pointer() : ptr{NULL}, index{0} {}
    pointer(node* p){
		ptr = p;
		index = 0;
	}
	pointer(node* p, int c){
		ptr = p;
		index = c;
	}
	bool compare(const pointer &p){
        return ptr == p.ptr && index == p.index;
	}
};

struct node{
	int val;
	atomic<pointer> nxt;

    node() : nxt{pointer{}} {}
};

struct msqueue{
	atomic<pointer> head;
	atomic<pointer> tail;

	msqueue() : head{new node{}}, tail{head.load().ptr} {}
} *Q;


bool enmsqueue(msqueue* q, int value){
    node* tempo = new node();
    tempo->val = value;
    tempo->nxt = pointer{NULL, 0};
    pointer tail, next;
    while(1){
        tail = q->tail.load();
        pointer head = q->head.load();
        next = tail.ptr->nxt;
        if(tail.index - head.index>=msq_size){
            delete tempo;
            return false;
        }	
        if(tail.compare(q->tail)){
            if(next.ptr == NULL){
            	pointer p(tempo, tail.index+1);
                if(tail.ptr->nxt.compare_exchange_weak(next, p)){
                    break;
                }
            }
            else{
            	pointer p(next.ptr, tail.index+1);
                q->tail.compare_exchange_weak(tail, p);
            }
        } 
    }
	pointer p(tempo, next.index+1);
    q->tail.compare_exchange_weak(tail, p);
    return true;
}

bool demsqueue(msqueue* q, int &pVal){
    pointer head;
    while(1){
        head = q->head.load();
        pointer tail = q->tail.load();
        pointer next = head.ptr->nxt;

        if(head.compare(q->head)){
            if(head.ptr == tail.ptr){
                if(next.ptr == NULL){
                    return false;
                }
            	pointer p(next.ptr, tail.index+1);
                q->tail.compare_exchange_weak(tail, p);
            }
            else{
                pVal = next.ptr->val; // before CAS otherwise dequeue may be done by other thread first
            	pointer p(next.ptr, next.index);
                if(q->head.compare_exchange_weak(head, p)){
                    break;
                }
            }
        }
    }
    delete head.ptr;
    return true;
}


void* p_f(void* tid){
    eq_ops++;
    int val = rand()%10;
    enmsqueue(Q,val);
}

void* c_f(void *tid){
    dq_ops++;
    int val = 0;
 	demsqueue(Q,val); 

    this_thread::sleep_for(chrono::microseconds(val));
}


int main(int argc, char** argv){
    cout.precision(10);
    msq_size = atoi(argv[1]);
    int p_count, c_count;
    p_count = atoi(argv[2]);
    c_count = atoi(argv[3]);

    Q = new msqueue();

    pthread_t producers[p_count];
    pthread_t consumers[c_count];
    auto start = chrono::steady_clock::now();
    for(long tid=0; tid<p_count; tid++)
        pthread_create(&producers[tid], NULL, p_f, (void*)tid);
    for(long tid=0; tid<c_count; tid++)
        pthread_create(&consumers[tid], NULL, c_f, (void*)tid);
    for(int i=0; i<p_count; i++)
        pthread_join(producers[i], NULL);
    for(int i=0; i<c_count; i++)
        pthread_join(consumers[i], NULL);
    auto end = chrono::steady_clock::now();
    auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start).count();
    double throughput = (double)((double)(eq_ops+dq_ops)/(double)elapsed);
    cout << throughput << endl;
    return 0;
}
