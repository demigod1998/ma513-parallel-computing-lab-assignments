// To run
// mpicc naive.c
// mpirun -n #process ./a.out #array_elements
// Assuming #array_elements/#process is an integer
#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
int compare(const void * a, const void * b) 
{ 
    return ( *(int*)a - *(int*)b ); 
}
int nextGap(int gap) 
{
// taken from gfg 
    if (gap <= 1) 
        return 0; 
    return (gap / 2) + (gap % 2); 
} 
void swapp(int *a,int *b)
{
	int t = *a;
	*a = *b;
	*b = t;
}
void merge(int *arr1, int *arr2, int n,int p,int ex) 
{
// taken from gfg
	int m = n;
    int i, j, gap = n + m; 
    for (gap = nextGap(gap); gap > 0; gap = nextGap(gap)) 
    { 
        // comparing elements in the first array. 
        for (i = 0; i + gap < n; i++) 
            if (arr1[i] > arr1[i + gap]) 
                swapp(&arr1[i], &arr1[i + gap]); 
  
        //comparing elements in both arrays. 
        for (j = gap > n ? gap-n : 0 ; i < n&&j < m; i++, j++) 
            if (arr1[i] > arr2[j]) 
                swapp(&arr1[i], &arr2[j]); 
  
        if (j < m) 
        { 
            //comparing elements in the second array. 
            for (j = 0; j + gap < m; j++) 
                if (arr2[j] > arr2[j + gap]) 
                    swapp(&arr2[j], &arr2[j + gap]); 
        } 
    } 


    if(p > ex)
    {
    	// keep the larger part of array if have larger rank
    	for(i = 0;i < n;i++)
    	{
    		*(arr1+i) = *(arr2+i);
    	}
    }
}
int main(int argc,char** argv)
{
	MPI_Init(&argc,&argv);
	int *arr,*recv,*temp;
	int n,p_count;
	int elements_per_p;
	MPI_Comm_size(MPI_COMM_WORLD,&p_count);

	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);

	if(pid == 0)
	{
		n = atoi(argv[1]);
		elements_per_p = n/p_count;
		for(int i = 1;i < p_count;i++)
		{
			MPI_Ssend(&elements_per_p,1,MPI_INT,i,0,MPI_COMM_WORLD);
		}
		arr = (int *)malloc(n*sizeof(int));
		srand(23);
		for(int i = 0;i< n;i++)
		{
			arr[i] = rand()%1000;
		}
		// printf("Original array : ");
		// for(int i = 0;i< n;i++)
		// {
		// 	printf("%d ",*(arr+i));
		// }
	
	}
	else
	{
		MPI_Recv(&elements_per_p,1,MPI_INT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}
	recv = (int *)malloc(elements_per_p*sizeof(int));
	// temp_f = (int *)malloc(2*elements_per_p*sizeof(int));
	temp = (int *)malloc(elements_per_p*sizeof(int));
	
	MPI_Scatter(arr,elements_per_p,MPI_INT,recv,elements_per_p,MPI_INT,0,MPI_COMM_WORLD);
	// printf("\nFor PE %d : elements are : ",pid);
	// for(int i = 0;i< elements_per_p;i++)
	// {
	// 	printf("%d ",*(recv+i));
	// }
	// printf("\n");
	qsort(recv, elements_per_p, sizeof(int), compare);
	// Now sorting shall commence
	//step1 : findout with which processed pi exchange data with
	int odd_ex, even_ex;
	odd_ex = even_ex = pid;
	if(pid%2)
	{
		odd_ex++;
		even_ex--;
	}
	else
	{
		odd_ex--;
		even_ex++;
	}
	// printf("reached here pe :%d,odd_ex :  %d,even_ex: %d\n",pid,odd_ex,even_ex);
	// if(odd_ex == -1 || odd_ex == p_count)
	// {
	// 	odd_ex = MPI_PROC_NULL;
	// }

	// if(even_ex == -1 || even_ex == p_count)
	// {
	// 	even_ex = MPI_PROC_NULL;
	// }

	//step2 : do p_count steps of odd_even sort
	double start_min,start_time = MPI_Wtime();
	for(int j = 0;j<p_count;j++)
	{
		// printf("reached here start pe :%d , j: %d\n",pid,j);
		if(j%2)
		{
			// printf("reached here first pe :%d , j: %d\n",pid,j);
			if(odd_ex!= -1 && odd_ex != p_count)
			{
				MPI_Sendrecv(recv,elements_per_p,MPI_INT,odd_ex,0,temp,elements_per_p,MPI_INT,odd_ex,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				merge(recv,temp,elements_per_p,pid,odd_ex);
				// if(pid > odd_ex)
				// {
				// 	// keep the large one
				// 	for(int i = 0;i < elements_per_p;i++)
				// 	{
				// 		recv[i] = recv[i+elements_per_p];
				// 	}

				// }
			}	
		}
		else
		{
	// printf("reached here 2nd pe :%d,odd_ex :  %d,even_ex: %d\n",pid,odd_ex,even_ex);
			if(even_ex!= -1 && even_ex != p_count)
			{
				// printf("check\n");
				MPI_Sendrecv(recv,elements_per_p,MPI_INT,even_ex,0,temp,elements_per_p,MPI_INT,even_ex,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
				// printf("check-2\n");
				merge(recv,temp,elements_per_p,pid,even_ex);
			}
		}

		// printf("reached here end pe :%d , j: %d\n",pid,j);
	}

	MPI_Gather(recv,elements_per_p,MPI_INT,	arr,elements_per_p,MPI_INT,0,MPI_COMM_WORLD);
	double end_time = MPI_Wtime();
	double end_max;

	MPI_Reduce(&start_time,&start_min,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD); 
    MPI_Reduce(&end_time,&end_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
	if(pid == 0)
	{
		// printf("\nfinal sorted array :");
		// for(int i = 0;i <n ;i++)
		// {
		// 	printf("%d ",*(arr+i));
		// }
		// printf("\n");
		double time_taken_peak = end_max - start_min;
        printf("%lf\n",time_taken_peak);
	}

	MPI_Finalize();
	return 0;
}
