#!/bin/bash
mpicc $1
for i in {1..8..1}
do
	mpirun -n "$(("2**${i}"))" ./a.out "$(("2**16"))" >> $2_processes.txt
done
for i in {4..16..2}
do
	mpirun -n 2 ./a.out "$(("2**${i}"))" >> $2_size.txt
done
wait
echo "DONE"
