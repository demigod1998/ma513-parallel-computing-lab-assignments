// To run
// mpicc recursive_sum.c
// mpirun -n #process ./a.out #array_elements
// assuming #process is a power of 2
// (#array_elements/#process) is integer
#include<mpi.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc,char **argv)
{
	//initializing the MPI environment
	MPI_Init(&argc,&argv);
	//taking input
	int n = atoi(argv[1]);
	int *arr;
	int elements_per_p;
	int ans;
	//to find number of processes
	int p_count;
	MPI_Comm_size(MPI_COMM_WORLD,&p_count);

	//to get process id(pid or rank)
	int pid;
	MPI_Comm_rank(MPI_COMM_WORLD,&pid);
	// distibuting the data
	if(pid == 0)
	{
		elements_per_p = n/p_count;// assuming n is perfectly divisible by p_count
		// elements_0_p = n - elements_per_p*p_count;//process 0 shall sum these
		arr = (int*)malloc(n*sizeof(int));
		for(int i = 0;i < n ;i++)
		{
			*(arr+i) = 1;
		}
		for(int i = 1; i < p_count; i++)
		{
			MPI_Ssend(&elements_per_p,1,MPI_INT,i,0,MPI_COMM_WORLD);
			MPI_Ssend(arr+(i)*elements_per_p,elements_per_p,MPI_INT,i,1,MPI_COMM_WORLD);
		}
	}
	else
	{
		MPI_Recv(&elements_per_p,1,MPI_INT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		arr = (int*)malloc(elements_per_p*sizeof(int));
		MPI_Recv(arr,elements_per_p,MPI_INT,0,1,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}
	// starts time only after distribution is complete
	MPI_Barrier(MPI_COMM_WORLD);
	double start_min,start_time = MPI_Wtime();
	// every process computes this sum locally
	int temp_sum = 0;
	for(int i = 0;i<elements_per_p; i++)
	{
		temp_sum += arr[i];
	}

	// now we need to concatenate the answer recursively
	long mul = 2;
	int received;
	while(mul <= p_count)
	{
		if((long)pid%mul != 0)
		{
			int d = pid - mul/2;
			MPI_Ssend(&temp_sum,1,MPI_INT,d,0,MPI_COMM_WORLD);
			break;
		}
		else
		{
			int s = pid+mul/2;
			MPI_Recv(&received,1,MPI_INT,s,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			temp_sum+= received;
		}
		mul *= 2;
	}
	
	double end_time = MPI_Wtime();
	double end_max;

	MPI_Reduce(&start_time,&start_min,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD); 
    MPI_Reduce(&end_time,&end_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

    if(pid==0){
        double time_taken_peak = end_max - start_min;
        printf("sum = %d , time = %lf\n",temp_sum,time_taken_peak);
    }

    MPI_Finalize();
}
